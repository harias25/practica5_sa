﻿using ClientesApi.Clases;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ClientesApi.Models
{
    public class Pedido
    {
        public int Id { get; set; }

        [Required()]
        public int Cliente { get; set; }

        [Required()]
        public int Restaurante { get; set; }

        [Required()]
        public int Menu { get; set; }

        Bitacora bitacora = new Bitacora();
        /// <summary>
        /// Metodo encargado de enviar un pedido al servicio de restaurantes e insetarlo en su base de datos
        /// </summary>
        /// <returns>Retorna un String con el Json del Resultado del Ingreso.</returns>
        public string sendPedido(){
            Utilidades utilidades = new Utilidades();
            var result = utilidades.executeAPI("POST", "http://localhost:8080/esb/ingresoRestaurante", JsonConvert.SerializeObject(this), "Basic c2E6U29mQWR2MjAyMA==");
            bitacora.escribirBitacora("Se asgina el pedido " + this.Id + " al restaurante " + this.Restaurante);
            bitacora.escribirBitacora("El resultado de la asignación fue: " + result);
            return result;
        }


        public string statusRestaurante(int pedido)
        {
            Utilidades utilidades = new Utilidades();
            var result = utilidades.executeAPI("GET", "http://localhost:8080/esb/consultRestaurante?pedido=" + pedido, null, "Basic c2E6U29mQWR2MjAyMA==");
            bitacora.escribirBitacora("Se consulta el estado del restaurante de el pedido " + pedido);
            bitacora.escribirBitacora("El resultado de la consulta fue: " + result);
            return result;
        }

        public string statusRepartidor(int pedido)
        {
            Utilidades utilidades = new Utilidades();
            var result = utilidades.executeAPI("GET", "http://localhost:8080/esb/consultRepartidor?pedido=" + pedido, null, "Basic c2E6U29mQWR2MjAyMA==");
            bitacora.escribirBitacora("Se consulta el estado del repartidor de el pedido " + pedido );
            bitacora.escribirBitacora("El resultado de la consulta fue: " + result);
            return result;
        }

    }
}
