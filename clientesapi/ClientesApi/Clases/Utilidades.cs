﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace ClientesApi.Clases
{
    /// <summary>
    /// Clase principal que manejara los diferentes metodos para la interacción con servicios
    /// </summary>
    class Utilidades
    {
        /// <summary>
        /// Metodo Principal para interactuar con un servicio y posteriormente devolverá un String con JSON obtenido
        /// </summary>
        /// <param name="method">Se recibe POST,GET,PUT,DELETE</param>
        /// <param name="link">URL del servicio con los parametros incluidos si los tuviera</param>
        /// <param name="data">contenido del body, si no se tiene enviar un null</param>
        /// <param name="token">Si se require algun metodo de autenticación se debe enviar, por defecto tiene valor null</param>
        /// <returns>Retorna un String con el JSON obtenido</returns>
        public String executeAPI(String method, String link, String data, String token = null)
        {
            String strResult = "";
            try
            {
                WebRequest request = WebRequest.Create(link);
                request.Method = method;

                request.ContentType = "application/json";
                if (token != null)
                {
                    request.Headers.Add("Authorization", token);
                }

                if (data == null)
                {
                    WebResponse response = request.GetResponse();
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string responseFromServer = reader.ReadToEnd();
                    strResult = responseFromServer;
                    reader.Close();
                    response.Close();
                }
                else
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(data);
                    request.ContentLength = byteArray.Length;
                    Stream dataStream = request.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    WebResponse response = request.GetResponse();
                    dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    string responseFromServer = reader.ReadToEnd().Replace(".0", "");
                    strResult = responseFromServer;
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                }
            }
            catch
            {
                Console.WriteLine("Incovenientes consumiendo el servicio!!!");
            }
            return strResult;
        }

    }

}
