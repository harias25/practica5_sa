﻿using RestauranteApi.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestauranteApi.Models
{
    /// <summary>
    /// Clase principal para el manejo de repartidores de los restaurantes
    /// </summary>
    public class Repartidor
    {
        [Required()]
        public int Id { get; set; }

        [Required()]
        public String Nombre { get; set; }

        /// <summary>
        /// Metodo que notifica al servicio de Repartidores que se le ha asignado un pedido
        /// </summary>
        /// <param name="pedido">Id del pedido que se va a asignar</param>
        /// <returns>Retorna un String con el Json del Resultado de la asignación</returns>
        public string Notificar(int pedido)
        {
            Utilidades utilidades = new Utilidades();
            var result = utilidades.executeAPI("POST", "http://localhost:8080/esb/ingresoRepartidor", "{\"Id\":"+pedido+ ",\"Repartidor\":"+this.Id+"}", "Basic c2E6U29mQWR2MjAyMA==");
            Bitacora bitacora = new Bitacora();
            bitacora.escribirBitacora("Se asgina el pedido " + pedido + " al repartidor " + Id);
            bitacora.escribirBitacora("El resultado de la asignación fue: " + result);
            return result;
        }

    }
}
