﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


/// <summary>
/// Clase para el manejo de una bitacora en un archivo de texto
/// </summary>
 class Bitacora
    {
        private String ruta = @"C:\LogSistemas\RepartidoresAPI";
        
        /// <summary>
        /// Metodo que escribe texto en un archivo del día de la bitacora que se desea imprimir
        /// </summary>
        /// <param name="texto">Texto que se insertará en el archivo</param>
        public void escribirBitacora(String texto)
        {
            try
            {
                var vrlPathFolderLog = ruta;
                if (!Directory.Exists(vrlPathFolderLog))
                {
                    DirectoryInfo di = Directory.CreateDirectory(vrlPathFolderLog);
                }

                var vrlFechaActual = DateTime.Now.ToString("yyyyMMdd");
                var vrlNombreArchivo = string.Format("{0}/{1}.txt", vrlPathFolderLog, vrlFechaActual);

                StreamWriter sw = new StreamWriter(vrlNombreArchivo, true);
                sw.WriteLine();
                sw.WriteLine(texto);
                sw.WriteLine();
                sw.Close();
            }
            catch{ }
            
        }

    /// <summary>
    /// Metodo que imprime una excepción en el archivo del día de bitacora
    /// </summary>
    /// <param name="exc">Excepción del error</param>
    /// <param name="Clase">Clase donde se obtuvo el error</param>
    /// <param name="Metodo">Metodo donde se obtuvo el Error</param>
        public void escribirBitacoraError(Exception exc, string Clase, string Metodo)
        {
            try
            {
                var vrlPathFolderLog = ruta;
                if (!Directory.Exists(vrlPathFolderLog))
                {
                    DirectoryInfo di = Directory.CreateDirectory(vrlPathFolderLog);
                }

                var vrlFechaActual = DateTime.Now.ToString("yyyyMMdd");
                var vrlNombreArchivo = string.Format("{0}/{1}.txt", vrlPathFolderLog, vrlFechaActual);

                StreamWriter sw = new StreamWriter(vrlNombreArchivo, true);
                sw.Write("******************** " + DateTime.Now);
                sw.WriteLine(" ********************");
                if (exc.InnerException != null)
                {
                    sw.Write("Inner Exception Type: ");
                    sw.WriteLine(exc.InnerException.GetType().ToString());
                    sw.Write("Inner Exception: ");
                    sw.WriteLine(exc.InnerException.Message);
                    sw.Write("Inner Source: ");
                    sw.WriteLine(exc.InnerException.Source);
                    if (exc.InnerException.StackTrace != null)
                        sw.WriteLine("Inner Stack Trace: ");
                    sw.WriteLine(exc.InnerException.StackTrace);
                }
                sw.Write("Exception Type: ");
                sw.WriteLine(exc.GetType().ToString());
                sw.WriteLine("Exception: " + exc.Message);
                sw.WriteLine("Clase: " + Clase);
                sw.WriteLine("Metodo: " + Metodo);
                sw.WriteLine("Stack Trace: ");
                if (exc.StackTrace != null)
                    sw.WriteLine(exc.StackTrace);
                sw.WriteLine();
                sw.WriteLine(" *********************************************************");
                sw.Close();
            }
            catch
            {

            }
            
        }

    }
