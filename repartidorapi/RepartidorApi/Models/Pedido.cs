﻿using Newtonsoft.Json;
using RepartidorApi.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RepartidorApi.Models
{

    /// <summary>
    /// Modelo principal para el manejo de pedidos de los repartidores
    /// </summary>
    public class Pedido
    {
        [Required()]
        public int Id { get; set; }

        [Required()]
        public int Repartidor { get; set; }

        Bitacora bitacora = new Bitacora();
        /// <summary>
        /// Metodo encargado de insertar el pedido en la base de datos de repartidores
        /// </summary>
        /// <returns>Retorna un String con el Json del Resultado del Ingreso.</returns>
        public string ingresarPedido(){
            Resultado resultado = new Resultado();
            resultado.CodigoResultado = Id;
            resultado.MensajeResultado = "Pedido Ingresado Correctamente!";
            bitacora.escribirBitacora("Se ingreso el pedido " + resultado.CodigoResultado);
            return JsonConvert.SerializeObject(resultado); 
        }


        /// <summary>
        /// Metodo que consulta el estado del pedido del repartidor asignado
        /// </summary>
        /// <param name="pedido">id del pedido a consultar el estado</param>
        /// <returns>Retorna un String con el Json del Resultado del Estado.</returns>
        public string statusPedido(int pedido)
        {
            Resultado resultado = new Resultado();
            var rand = new Random();
            resultado.CodigoResultado = rand.Next(1, 4);
            if (resultado.CodigoResultado == 1)
            {
                resultado.MensajeResultado = "Pedido Recibido!";
            }
            else if (resultado.CodigoResultado == 2)
            {
                resultado.MensajeResultado = "Pedido En Camino!";
            }
            else
            {
                resultado.MensajeResultado = "Pedido Entregado!";
                entregarPedido(pedido);
            }
            bitacora.escribirBitacora("El estado del pedido " + pedido + " es "+resultado.MensajeResultado);
            return JsonConvert.SerializeObject(resultado);
        }


        /// <summary>
        /// Metodo que actualiza el estado del pedido a entregado
        /// </summary>
        /// <param name="pedido">Id del pedido a actualizar.</param>
        public void entregarPedido(int pedido)
        {
            Console.WriteLine("Se actualiza el pedido " + pedido + " como entregado a la hora y fecha " + DateTime.Now);
            bitacora.escribirBitacora("Se actualiza el pedido " + pedido + " como entregado a la hora y fecha " + DateTime.Now);
        }


    }
}
