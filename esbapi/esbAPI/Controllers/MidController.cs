﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

/// <summary>
/// Controlador principal para manejar los procedimientos de las acciones para un repartidor y sus pedidos
/// </summary>
namespace esbAPI.Controllers
{
    [Consumes("application/json", "application/json-patch+json", "multipart/form-data")]
    [Route("api/[controller]")]
    public class MidController : Controller
    {

        /// <summary>
        /// Metodo GET que unicamente retorna un texto para validar si el servicio esta activo
        /// </summary>
        /// <returns>Retorna un texto con un mensaje del nombre del servicio y su fecha</returns>
        [HttpGet("/status")]
        public string status()
        {
            return "Microservicio de ESB - Software Avanzado Segundo Semestre 2020 - " + DateTime.Now.ToString();
        }


        /// <summary>
        /// Metodo POST general para la recepcion de solicitud de este tipo, se tiene el valor {slug} como el verbo del servicio que se va a consumir
        /// </summary>
        /// <param name="slug">Verbo del nombre maquillado del servicio que se desea consumir</param>
        /// <param name="bodyContent">Contenido del POST obtenido</param>
        /// <returns></returns>
        [HttpPost, Route("/esb/{slug}")]
        public async Task<IActionResult> UrlPost(String slug, [FromBody] JObject bodyContent)
        {
            Dictionary<string, string> slugs = new Dictionary<string, string>();

            string url_clientes = "http://localhost:8081";
            string url_restaurantes = "http://localhost:8082";
            string url_repartidores = "http://localhost:8083";

            
            //====================================================================================================================================================================
            //Creación del diccionario para poder asignar los pedidos
            //====================================================================================================================================================================
            slugs["ingresoCliente"] = url_clientes + "/IngresarPedido";              //crea el pedido en la solicitud de un cliente
            slugs["ingresoRestaurante"] = url_restaurantes + "/IngresarPedido";      //crea el pedido en el restaurante
            slugs["ingresoRepartidor"] = url_repartidores + "/IngresarPedido";       //asigna el pedido al repartidor
            //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            //Define your base url
            string baseURL = slugs[slug];
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(String.Format("http://{0}", "localhost"));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "c2E6U29mQWR2MjAyMA==");
                    HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(bodyContent), Encoding.UTF8);
                    httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    using (HttpResponseMessage res = await client.PostAsync(baseURL, httpContent))
                    {
                        using (HttpContent content = res.Content)
                        {
                            string data = await content.ReadAsStringAsync();

                            if (data != null)
                            {
                                return Content(data, "application/json");
                            }
                            else
                            {
                                return Content("False");
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                return Content(exception.Message);
            }
        }

        /// <summary>
        /// Metodo GET general para la recepcion de solicitud de este tipo, se tiene el valor {slug} como el verbo del servicio que se va a consumir
        /// </summary>
        /// <param name="slug">Verbo del nombre maquillado del servicio que se desea consumir</param>
        /// <param name="pedido">Identificador del Pedido recibido</param>
        /// <returns></returns>
        [HttpGet, Route("/esb/{slug}")]
        public async Task<IActionResult> UrlGet(String slug, int pedido)
        {
            Dictionary<string, string> slugs = new Dictionary<string, string>();

            string url_clientes = "http://localhost:8081";
            string url_restaurantes = "http://localhost:8082";
            string url_repartidores = "http://localhost:8083";


            //====================================================================================================================================================================
            //Creación del diccionario para poder asignar los pedidos
            //====================================================================================================================================================================
            slugs["consultClietRest"] = url_clientes + "/getStatusRestaurante?pedido="+pedido;              //Consulta el estado del pedido en el restaurante desde el servicio de clientes
            slugs["consultClietRep"] = url_clientes + "/getStatusRepartidor?pedido=" + pedido;              //Consulta el estado del pedido con el repartidor desde el servicio de clientes
            slugs["consultRestaurante"] = url_restaurantes + "/getStatusPedido?pedido=" + pedido;           //Consulta el estado del pedido al restaurante
            slugs["consultRepartidor"] = url_repartidores + "/getStatusPedido?pedido=" + pedido;            //Consulta el estado del pedido al repartidor
            //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            //Define your base url
            string baseURL = slugs[slug];
            try
            {
                WebRequest request = WebRequest.Create(baseURL);
                request.Method = "GET";
                request.Headers.Add("Authorization", "Basic c2E6U29mQWR2MjAyMA==");
                request.ContentType = "application/json";
                WebResponse response = request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string data = reader.ReadToEnd();
                reader.Close();
                response.Close();


                if (data != null)
                {
                    return Content(data, "application/json");
                }
                else
                {
                    return Content("False");
                }

            }
            catch (Exception exception)
            {
                return Content(exception.Message);
            }
        }

    }
}
