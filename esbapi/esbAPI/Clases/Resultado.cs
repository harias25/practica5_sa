﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace esbAPI.Clases
{
    /// <summary>
    /// Clase utilizada para poder retornar un codigo y resultado de la ejecución o llamada del servicio, siendo este un resultado correcto o una excepción en el sistema
    /// </summary>
    public class Resultado
    {
        private int codigoResultado;
        private String mensajeResultado;


        public int CodigoResultado
        {
            get
            {
                return codigoResultado;
            }

            set
            {
                codigoResultado = value;
            }
        }

        public string MensajeResultado
        {
            get
            {
                return mensajeResultado;
            }

            set
            {
                mensajeResultado = value;
            }
        }
    }
}
